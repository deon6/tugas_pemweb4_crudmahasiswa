<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
    public function home() {
        $mahasiswa = DB::table('mahasiswa')->get();        
        return view('home', ['mahasiswa' => $mahasiswa]);  
    }

    public function tambah() {
        return view('tambah');
    }

    public function insert(Request $request) {
        DB::table('mahasiswa')->insert([
            'nama' => $request->nama,
            'nim' => $request->nim,
            'kelas' => $request->kelas,
            'prodi' => $request->prodi,
            'fakultas' => $request->fakultas

        ]);
        return redirect('/');
    }

    public function edit($id) {
        
        $mahasiswa = DB::table('mahasiswa')->where('id', $id)->get();
        return view('edit', ['mahasiswa' => $mahasiswa]);
    }

    public function update(Request $request) {
        DB::table('mahasiswa')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'nim' => $request->nim,
            'kelas' => $request->kelas,
            'prodi' => $request->prodi,
            'fakultas' => $request->fakultas
        ]);
        return redirect('/');
    }

    public function hapus($id) {
        
        DB::table('mahasiswa')->where('id', $id)->delete();
        return redirect('/');

    }
}
