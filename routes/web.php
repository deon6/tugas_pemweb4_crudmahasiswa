<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [App\Http\Controllers\MahasiswaController::class, 'home']);

Route::get('/tambah', [App\Http\Controllers\MahasiswaController::class, 'tambah']);

Route::post('/insert', [App\Http\Controllers\MahasiswaController::class, 'insert']);

Route::get('/edit/{id}', [App\Http\Controllers\MahasiswaController::class, 'edit']);

Route::post('/update', [App\Http\Controllers\MahasiswaController::class, 'update']);

Route::get('/hapus/{id}', [App\Http\Controllers\MahasiswaController::class, 'hapus']);

